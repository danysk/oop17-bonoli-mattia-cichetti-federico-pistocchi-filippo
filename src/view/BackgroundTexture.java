package view;

/**
 * Enumeration that contains usable names for the objects in the view and their corresponding
 * Image files paths so that it is easy to modify paths or add more textures to the game.
 */
public enum BackgroundTexture {
    /**
     * Request for the last lane image.
     */
    LANE_END("/img/textures/endlane.png"),
    /**
     * Request for the starting safe lane image.
     */
    LANE_FIRST("/img/textures/firstlane.png"),
    /**
     * Request for the middle safe image.
     */
    LANE_MID("/img/textures/safemidlane.png"),
    /**
     * Request for the street image.
     */
    STREET("/img/textures/streetlane.png"),
    /**
     * Request for the water image.
     */
    WATER("/img/textures/waterlane.png"),

    /**
     * request for the background image.
     */
    BACKGROUND("/img/textures/background.png");

    private final String path;

    BackgroundTexture(final String str) {
        this.path = str;
    }

    /**
     * Method that gets the actual URL of the file location.
     * @return the requested file URL.
     */
    public String getUrl() {
        return this.path;
    }

}
