package view.menu;

import javafx.beans.binding.Bindings;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import model.GameDifficult;
import utilities.Constants;
import view.AbstractScene;
import view.View;
import view.ViewImpl;

/**
 * layout of optionMenu.
 */
public class OptionMenu extends AbstractScene {

    private boolean resizable = ViewImpl.RES_DEFAULT; //NOPMD 
    /* Private field 'resizable' could be made final; it is only initialized in the declaration or constructor.
     * This field can't be final.
     */

    private static final double WIDTH_DIV = 5.0;
    private static final int VGAP_DIV = 20;
    private static final int HGAP_DIV = 10;
    private static final double BUTTON_WIDTH = 4.0;
    private static final double LABEL_WIDTH = 2.0;
    private static final int FIRST_COLUMN = 0;
    private static final int FIRST_ROW = 0;
    private static final int SECOND_COLUMN = 1;
    private static final int SECOND_ROW = 1;
    private static final int THIRD_ROW = 2;
    private static final int FIFTH_ROW = 4;
    private static final int SIXTH_ROW = 5;
    private static final int FPS_SEL_1 = 60;
    private static final int FPS_SEL_2 = 30;
    /**
     * @param width of the scene
     * @param height of the scene
     * @param v view reference
     */
    public OptionMenu(final double width, final double height, final View v) {
        super(new BorderPane(), width, height);
        final BorderPane mainPane = (BorderPane) getRoot();
        final View view = v;
        final GridPane pane = new GridPane();
        final Label saved = new Label("SAVED");
        final Label title = new Label("OPTIONS");
        final Button back = new Button("MENU");
        final Label resLabel = new Label("Resizable Window");
        final CheckBox resOpt = new CheckBox();
        final Label difLabel = new Label("Difficulty Level");
        final ChoiceBox<GameDifficult> difOpt = new ChoiceBox<>();
        final Label fpsLabel = new Label("FPS");
        final ChoiceBox<Integer> fpsOpt = new ChoiceBox<>();
        final Button save = new Button("SAVE");
        resOpt.setSelected(false);
        fpsOpt.getItems().addAll(FPS_SEL_1, FPS_SEL_2);
        difOpt.getItems().addAll(GameDifficult.values());
        pane.styleProperty().bind(Bindings.format("-fx-font-size: %.2fpt;", super.getFontSize()));
        back.prefWidthProperty().bind(this.widthProperty().divide(BUTTON_WIDTH));
        save.prefWidthProperty().bind(this.widthProperty().divide(BUTTON_WIDTH));
        resLabel.prefWidthProperty().bind(this.widthProperty().divide(LABEL_WIDTH));
        resOpt.prefWidthProperty().bind(this.widthProperty().divide(WIDTH_DIV));
        difLabel.prefWidthProperty().bind(this.widthProperty().divide(LABEL_WIDTH));
        difOpt.prefWidthProperty().bind(this.widthProperty().divide(WIDTH_DIV));
        fpsLabel.prefWidthProperty().bind(this.widthProperty().divide(LABEL_WIDTH));
        fpsOpt.prefWidthProperty().bind(this.widthProperty().divide(WIDTH_DIV));
        pane.hgapProperty().bind(this.widthProperty().divide(HGAP_DIV));
        pane.vgapProperty().bind(this.heightProperty().divide(VGAP_DIV));
        title.styleProperty().bind(Bindings.format("-fx-font-size: %.2fpt;", this.widthProperty().divide(WIDTH_DIV)));
        mainPane.setTop(title);
        mainPane.setCenter(pane);
        BorderPane.setAlignment(title, Pos.CENTER);
        title.setId("title-label");
        pane.setId("optionPane");
        difOpt.getSelectionModel().select(Constants.DIFF_DEFAULT);
        fpsOpt.getSelectionModel().select(fpsOpt.getItems().get(fpsOpt.getItems().indexOf(Constants.FPS_DEFAULT)));
        pane.setAlignment(Pos.CENTER); 
        pane.add(resLabel, FIRST_COLUMN, FIRST_ROW);
        pane.add(resOpt, SECOND_COLUMN, FIRST_ROW);
        pane.add(difLabel, FIRST_COLUMN, SECOND_ROW);
        pane.add(difOpt, SECOND_COLUMN, SECOND_ROW);
        pane.add(fpsLabel, FIRST_COLUMN, THIRD_ROW);
        pane.add(fpsOpt, SECOND_COLUMN, THIRD_ROW);
        pane.add(back, FIRST_COLUMN, FIFTH_ROW);
        pane.add(save, SECOND_COLUMN, 4);
        pane.add(saved, SECOND_COLUMN, SIXTH_ROW);
        GridPane.setHalignment(resOpt, HPos.RIGHT);
        GridPane.setHalignment(difOpt, HPos.RIGHT);
        GridPane.setHalignment(fpsOpt, HPos.RIGHT);
        GridPane.setHalignment(saved, HPos.CENTER);
        resOpt.setSelected(resizable);
        saved.setVisible(false);
        back.setOnAction(e -> {
            view.changeScene(ViewImpl.GameScreen.MAINMENU);
            saved.setVisible(false);
        });
        save.setOnAction(e -> {
            if (difOpt.getSelectionModel().getSelectedItem() != null) {
                view.getController().setDifficult(difOpt.getSelectionModel().getSelectedItem());
            }
            if (fpsOpt.getSelectionModel().getSelectedItem() != null) {
                view.getController().setFPS(fpsOpt.getSelectionModel().getSelectedItem());
            }
            view.setResizable(resizable);
            saved.setVisible(true);
        });
        resOpt.selectedProperty().addListener(e -> {
            resizable = !resizable;
            view.setResizable(resizable);
        });
}

    @Override
    public void initialize() { }
}
