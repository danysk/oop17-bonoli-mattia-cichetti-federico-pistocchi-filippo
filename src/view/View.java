package view;

import java.util.List;
import java.util.Optional;

import controller.Controller;
import javafx.scene.input.KeyCode;
import view.game.ViewableEntity;

/**
 * View interface.
 */
public interface View {
    /**
     * start the menu showing the main menu.
     */
    void startMenu();

    /**
     * change the current displayed scene.
     * @param s to be displayed.
     * @return current displayed scene after change
     */
    AbstractScene changeScene(ViewImpl.GameScreen s);
    /**
     * set controller reference in view.
     * @param c controller
     */
    void setController(Controller c);
    /**
     * getter for controller.
     * @return controller
     */
    Controller getController();

    /**
     * draw method to render objects.
     * @param l list of updated entities
     */
    void draw(List<ViewableEntity> l);

    /**
     * draw method to render dens.
     * @param dens list of den positions and whether they are free or not (isFree)
     */
    void drawDens(List<ViewableEntity> dens);

    /**
     * @param newRemainingTime percent of time left.
     */
    void updateTimer(double newRemainingTime);

    /**
     * Updates the score label.
     * @param newScore 
     */
    void updateScore(int newScore);

    /**
     * Updates the Lives panel.
     * @param newLives The new lives value.
     */
    void updateLives(int newLives);

    /**
     * Gets the first input from a list of inputs collected during a single frame.
     * @return the first input.
     */
    Optional<KeyCode> getInput();

    /**
     * Adds an input to the input list.
     * @param k The input's KeyCode
     */
    void addInput(KeyCode k);

    /**
     * set the stage resizable.
     * @param resizable property
     */
    void setResizable(boolean resizable);

    /**
     * Updates the mod text by showing the name of the most recent activated mod.
     * @param s The name of the mod.
     */
    void setModText(String s);

    /**
     * Clears the mod text.
     */
    void clearModText();
}
