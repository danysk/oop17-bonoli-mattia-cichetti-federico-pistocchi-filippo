package view.sound;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javafx.application.Platform;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import view.View;
import view.ViewImpl.GameScreen;

/**
 * A class that manages the reproduction of sounds.
 */
public class SoundManagerImpl implements SoundManager {

    private final Map<GameScreen, MediaPlayer> links;
    private Optional<MediaPlayer> currentlyPlaying;
    private final MediaPlayer winFanfare;

    /**
     * Default constructor.
     * @param v The view this SoundManager is attached to.
     */
    public SoundManagerImpl(final View v) {
        this.links = new HashMap<>();
        /****** DEFAULT INITIALIZATION ******/
        links.put(GameScreen.GAME, new MediaPlayer(new Media(this.getClass().getResource("/sound/fronett.mp3").toExternalForm())));
        links.put(GameScreen.ENDMENU, new MediaPlayer(new Media(this.getClass().getResource("/sound/you_have_died.mp3").toExternalForm())));
        links.put(GameScreen.MAINMENU, new MediaPlayer(new Media(this.getClass().getResource("/sound/menu.mp3").toExternalForm())));
        links.entrySet().forEach(e -> e.getValue().setCycleCount(Integer.MAX_VALUE));

        /****** PERSONAL SETTINGS ******/
        links.get(GameScreen.ENDMENU).setCycleCount(1);
        this.winFanfare = new MediaPlayer(new Media(this.getClass().getResource("/sound/win.mp3").toExternalForm()));
        this.winFanfare.setOnEndOfMedia(() -> winFanfare.stop());
        Platform.runLater(() -> v.getController().getGameLogic().registerAdvancedObserver(i -> playWin()));

        currentlyPlaying = Optional.empty();
    }

    /**/
    @Override
    public void play(final GameScreen s) {
        final Thread player = new Thread(() -> {
            if (links.containsKey(s)) {
                if (currentlyPlaying.isPresent()
                && !currentlyPlaying.get().equals(links.get(s))) {
                    currentlyPlaying.get().stop();
                }
                currentlyPlaying = Optional.of(links.get(s));
                currentlyPlaying.get().play();
            }
        });
        player.start();
    }

    /**/
    @Override
    public void playWin() {
        final Thread winPlayer = new Thread(() -> {
            if (currentlyPlaying.isPresent()) {
                currentlyPlaying.get().stop();
            }
            winFanfare.play();
            if (!currentlyPlaying.get().equals(links.get(GameScreen.ENDMENU))) {
                play(GameScreen.GAME);
            }
        });
        winPlayer.start();
    }

    /**/
    @Override
    public void end() {
        links.entrySet().forEach(m -> m.getValue().stop());
    }
}
