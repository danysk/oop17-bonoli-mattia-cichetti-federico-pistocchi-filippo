package controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javafx.application.Platform;
import model.lane.LaneWithObstacles;
import utilities.Constants;
import model.GameDifficult;
import model.GameLogic;
import utilities.Direction;
import utilities.GameObserver;
import view.View;
import view.ViewImpl.GameScreen;
import view.game.ViewableEntity;

/**
 * game loop.
 */
public class GameLoop extends Thread {

    private enum State {
        READY, RUNNING, PAUSED, STOPPED
    }

    private static final int NS_PER_SECOND = 1000000000;
    private static final int NS_PER_MS = 1000000;
    private static final int UPDATES_PER_SECOND = 25;
 
    private final int fps;
    private final View view;
    private volatile State state;
    private final GameLogic game;
    private final Set<GameObserver> observers;
    private final LeaderboardManager leaderboardManager;
    private final ModManager modManager;

    /**
     * @param fps 
     * @param v view
     * @param d difficult
     * @param g gamelogic
     * @param lm leaderboard manager
     */
    public GameLoop(final int fps, final View v, final GameDifficult d, final GameLogic g, final LeaderboardManager lm) {
        super();
        this.fps = fps;
        view = v;
        state = State.READY;
        game = g;
        leaderboardManager = lm;
        game.setDifficult(d);
        observers = new HashSet<>();
        this.modManager = new ModManagerImpl(this, 100);
    }
 
    /**
     * core method.
     */
    public void run() {
        int counter = 0;
        //maximum time between two renderings
        final double timePerFrame = NS_PER_SECOND / fps;
        //maximum time between two logic updates
        final double timePerUpdate = NS_PER_SECOND / UPDATES_PER_SECOND;
        //game time behind real time
        double lag = 0.0;
        double startingTime = System.nanoTime();

        while (!(state.equals(State.STOPPED))) {
            if (!state.equals(State.PAUSED)) {
                //game is active
                double current = System.nanoTime();
                //time passed between two loop
                final double timeElapsed = current - startingTime;
                lag += timeElapsed;
                startingTime = current;
                //input handling
                game.registerInput(view.getInput());
                //update model
                while (lag >= timePerUpdate) {
                    game.update();
                    counter++;
                    if (counter == UPDATES_PER_SECOND) {
                        game.advanceTime(1);
                        counter = 0;
                    }
                    if (game.gameOver()) {
                        modManager.clean();
                        game.detachObservers();
                        observers.clear();
                        leaderboardManager.setPartialScore(game.getScoreManager().getScore());
                        Platform.runLater(() -> view.changeScene(GameScreen.ENDMENU).initialize());
                        this.stopGameLoop();
                    }
                    lag -= timePerUpdate;
                }
                //building entities to be drawn
                final List<ViewableEntity> entityToDraw = new ArrayList<>();
                final List<ViewableEntity> densToDraw = new ArrayList<>();
                for (int l = 0; l < game.getWorld().getLane().size(); l++) {
                    final int lane = l;
                    if (game.getWorld().getLane().get(lane) instanceof LaneWithObstacles) {
                        game.getWorld().getLane().get(lane).getObstacle().forEach(o -> {
                            entityToDraw.add(new ViewableEntity(o.getGameObjectType(), 
                                                o.getCenter(),
                                                lane, game.getWorld().getLane().get(lane)
                                                .getSpeed() > 0 ? Direction.FACING_RIGHT : Direction.FACING_LEFT));
                        });
                    }
                    game.getWorld().getLane().get(lane).getMods().forEach(m -> {
                        entityToDraw.add(new ViewableEntity(m.getGameObjectType(),
                                m.getCenter(), lane, 
                                Direction.FACING_LEFT));
                    });
                }
                game.getWorld().getDen().forEach(d -> densToDraw.add(new ViewableEntity(
                        d.getGameObjectType(), d.getCenter(), Constants.WORLD_NUMBER_OF_LANE - 1, 
                                Direction.FACING_RIGHT)));

                entityToDraw.add(new ViewableEntity(game.getFrog().getGameObjectType(), 
                        game.getFrog().getCenter(), 
                        game.getFrog().getOccupiedLane(), game.getFrog().getFacing()));

                //drawing entities
                view.draw(entityToDraw);
                view.drawDens(densToDraw);
                view.updateTimer((double) game.getTime() / Constants.STARTING_GAME_TIME);
                view.updateLives(game.getFrog().getLives());
                view.updateScore(game.getScoreManager().getScore());
                //update observers
                observers.forEach(o -> o.update(null));
                current = System.nanoTime();
                try {
                    while (current - startingTime < timePerFrame) {
                        sleep(1);
                        current = System.nanoTime();
                    }
                } catch (InterruptedException e) { }
            } else {
                //game is paused
                try {
                  //nullify input if caught when paused
                    view.getInput();
                    sleep((long) timePerFrame / NS_PER_MS);
                    startingTime = System.nanoTime();
                } catch (InterruptedException e) { }
            }
        }
    }

    /**
     * stop the game loop.
     */
    public synchronized void stopGameLoop() {
        this.state = State.STOPPED;
    }

    /**
     * pause the game loop.
     */
    public synchronized void pauseGameLoop() {
        state = State.PAUSED;
    }

    /**
     * start the game loop.
     */

    public synchronized void startGameLoop() {
        game.initializeGame(UPDATES_PER_SECOND);
        state = State.RUNNING;
        this.start();
    }

    /**
     * resume game loop.
     */
    public synchronized void resumeGameLoop() {
        state = State.RUNNING;
    }

    /**
     * Simple getter for the GameLogic this GameLoop is updating.
     * @return GameLogic
     */
    public GameLogic getGame() {
        return this.game;
    }

    /**
     * Simple getter for the View this GameLoop is updating.
     * @return view
     */
    public View getView() {
        return this.view;
    }

    /**
     * Registers a GameObserver to the list of observers that is updated once per tick.
     * @param o The GameObserver to register.
     */
    public void registerObserver(final GameObserver o) {
        this.observers.add(o);
    }

    /**
     * getter for current gameloop status.
     * @return state of the gameloop
     */
    public boolean isActive() {
        return state.equals(State.RUNNING) || state.equals(State.PAUSED) ? true : false;
    }
}
