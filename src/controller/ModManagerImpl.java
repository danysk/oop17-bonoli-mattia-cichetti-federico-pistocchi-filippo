package controller;

import utilities.Constants;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import model.GameLogic;
import model.gameobject.GameObjectImpl.GameObjectType;
import model.mod.ModAlreadyPresentException;
import model.mod.ModEntity;
import model.mod.ModObstacle;
import model.mod.ModType;

/**
 * The default implementation for the ModManager.
 *
 */
public class ModManagerImpl implements ModManager {

    private static final int MOD_LIMIT = 4;
    private static final double MOD_SPAWN_RATE = 1 / 3;
    private static final int TEXT_UPDATE_FRAME_AFTER = 250;

    private int counter;
    private int textUpdateFrame;
    private final int modDelay;
    private int modsOnStage;
    private final Map<ModObstacle, Integer> activeMods;
    private final GameLogic game;
    private final RunnableWithArgument showText;
    private final Runnable cleartText;

    /**
     * Constructor that adds two listeners to GameLoop and CollisionManager components.
     * @param g the GameLoop
     * @param defaultModDelay The delay between the spawning of two mods. It changes with difficulty.
     */
    public ModManagerImpl(final GameLoop g, final int defaultModDelay) {
        showText = m ->  g.getView().setModText(m);
        cleartText = () -> g.getView().clearModText();
        this.activeMods = new HashMap<>();
        g.registerObserver(i -> this.tick());
        g.getGame().registerObserver(m ->  this.activateMod((ModObstacle) m));
        g.getGame().registerAdvancedObserver(i -> this.levelUP());
        this.game = g.getGame();
        this.modDelay = defaultModDelay;
        this.counter = 0;
        this.modsOnStage = 0;
        this.textUpdateFrame = 0;
    }

    private void levelUP() {
        this.counter = 0;
        this.modsOnStage = 0;
        this.textUpdateFrame = 0;
        clean();
    }

    /**/
    @Override
    public void activateMod(final ModObstacle m) {
        this.activeMods.put(m, m.getModType().getTimeActive());
        m.getModType().getMod().execute(game);
        this.modsOnStage--;
        showOnView(m.getModType());
        textUpdateFrame = counter + TEXT_UPDATE_FRAME_AFTER;
    }

    /**/
    @Override
    public void tick() {
        counter++;
        if (!this.activeMods.isEmpty()) {
            this.activeMods.entrySet().forEach(e -> e.setValue(e.getValue() - 1));
            final Map<ModObstacle, Integer> toFix = this.activeMods.entrySet().stream()
                                                           .filter(e -> e.getValue() == 0)
                                                           .collect(Collectors.toMap(p -> p.getKey(), 
                                                                                   p -> p.getValue()));
            if (!toFix.isEmpty()) {
                toFix.entrySet().forEach(e -> { 
                    e.getKey().getModType().getFixer().execute(game);
                    activeMods.remove(e.getKey());
                });
            }
        }
        if (counter == textUpdateFrame) {
            clearView();
        }
        if (counter % modDelay == 0 && modsOnStage < MOD_LIMIT) {
            tryAddingMod();
        }
    }

    private void tryAddingMod() {
        final Random rand = new Random();
        double spawnChance = rand.nextDouble();
        if (spawnChance > MOD_SPAWN_RATE) {
            spawnChance = rand.nextDouble();
            final int chosenTypeIndex = rand.nextInt(Constants.MODARRAY.length);
            final ModType chosenType = Constants.MODARRAY[chosenTypeIndex];
            if (spawnChance >= (1 - chosenType.getChance())) {
                final int chosenLane = rand.nextInt(game.getWorld().getLane().size() - 1);
                final double center = rand.nextDouble() 
                        * (Constants.WORLD_RIGHT_LIMIT - (GameObjectType.MOD.getWidth() / 2));
                try {
                    game.getWorld().getLane().get(chosenLane).addMod(
                            new ModEntity(GameObjectType.MOD.create(center), chosenType));
                    this.modsOnStage++;
                } catch (ModAlreadyPresentException e) {
                    //do nothing, maybe next time...
                }
            }
        }
    }

    /**/
    @Override
    public void clean() {
        activeMods.entrySet().stream().forEach(m -> m.getKey().getModType().getFixer().execute(game));
        activeMods.clear();
        clearView();
    }

    private void showOnView(final ModType m) {
       showText.run(m.name());
    }

    private void clearView() {
       cleartText.run();
    }

    private interface RunnableWithArgument {
        void run(String s);
    }
}
