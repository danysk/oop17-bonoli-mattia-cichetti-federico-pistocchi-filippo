package controller;

import model.mod.ModObstacle;

/**
 * A ModManager is used to keep Mods in execution for long periods of time.
 */
public interface ModManager {

    /**
     * This method adds a Mod to the current Set of active Mods.
     * @param m The mod to be activated.
     */
    void activateMod(ModObstacle m);

    /**
     * Tick is called once per GameLoop cycle and decreases the time-to-live of each active Mod.
     */
    void tick();

    /**
     * Cleans all active mods.
     */
    void clean();
}
