package controller.movement;

import java.util.Optional;

import javafx.scene.input.KeyCode;
import model.frog.Frog;

/**
 * Receives the user inputs and calls the relative commands.
 */

public class Invoker {
    private final MoveUp moveUp;
    private final MoveDown moveDown;
    private final MoveLeft moveLeft;
    private final MoveRight moveRight;
    private final Cheat cheat;

    /**
     * 
     */
    public Invoker() {
 
        moveUp = new MoveUp();
        moveDown = new MoveDown();
        moveLeft = new MoveLeft();
        moveRight = new MoveRight();
        cheat = new Cheat();
    }

    /**
     * Executes commands according to user input.
     * @param frog the frog to execute the commands on
     * @param inputs the user inputs
     */
    public void invoke(final Frog frog, final Optional<KeyCode> inputs) {
        if (inputs.isPresent()) {
            switch (inputs.get()) {
                case F1: cheat.execute(frog);
                             break;

                case W: moveUp.execute(frog);
                        break;

                case A: moveLeft.execute(frog);
                        break;

                case S: moveDown.execute(frog);
                        break;

                case D: moveRight.execute(frog);
                        break;

                default: break;
            }
        }
    }

}
