package model.gameobject;

import java.util.Iterator;

/**
 * This interface is used to calculate centers of a sequence of obstacle in a lane.
 */
public interface CentersIterator extends Iterator<Double> {

}
