package model.frog;

import model.gameobject.GameObject;
import model.gameobject.ObstacleBaseType;
import model.gameobject.GameObjectImpl.GameObjectType;
import utilities.Constants;

/**
* Decorator to add functionality required for the frog to a GameObject.
*/
public abstract class GameObjDecorator implements GameObject {

    private final GameObject gameObject;
    private int occupiedLane;
    private double speed;
    private int lives;

    /**
    * Wrapper for the GameObject, sets own fields while delegating common fields to the GameObject interface.
    * @param occupiedLane index of the lane the object is in, 0 being the top most one
    * @param speed speed at which the object will move once the updatePosition is called, used while the frog is on the logs
    * @param gameObject is the gameObject to be decorated
    */
    public GameObjDecorator(final int occupiedLane, final double speed, final GameObject gameObject) {
        this.gameObject = gameObject;
        this.speed = speed;
        this.occupiedLane = occupiedLane;
    }

    /**
     *@return the center of the object
     */
    public double getCenter() {
        return gameObject.getCenter();
    }

    /**
    *@param newCenter the center to be set
    */
    public void setCenter(final double newCenter) {
        if (newCenter < 0 || newCenter > Constants.WORLD_RIGHT_LIMIT) {
            throw new IllegalArgumentException("Object cannot be moved out of bounds");
        } else {
        gameObject.setCenter(newCenter);
        }
    }

    /**
    *@return the ObjectType
    */
    public GameObjectType getGameObjectType() {
        return gameObject.getGameObjectType();
    }

    /**
    *@return the width of the object
    */
    public double getWidth() {
        return gameObject.getWidth();
    }

    /**
    *@return the base type of the object 
    */
    public ObstacleBaseType getBaseType() {
        return gameObject.getBaseType();
    }

    /**
    *@return the lane the object is in
    */
    public int getOccupiedLane() {
        return this.occupiedLane;
    }

    /**
    *@param occupiedLane sets the lane the object is in
    */
    public void setOccupiedLane(final int occupiedLane) {
        if (occupiedLane < 0 || occupiedLane > Constants.WORLD_NUMBER_OF_LANE) {
            throw new IllegalArgumentException("Object cannot be moved on a non-exixting lane");
        } else {
            this.occupiedLane = occupiedLane;
        }
    }

    /**
    *@return the speed the object will move every time updatePosition is called
    */
    public double getSpeed() {
        return this.speed;
    }

    /**
    *@param speed sets the speed the object will move every time updatePosition is called
    */
    public void setSpeed(final double speed) {
        this.speed = speed;
    }

    /**
    * Move the object based on the set position.
    */
    public void udpatePosition() {
        if ((gameObject.getCenter() + this.speed) >= Constants.WORLD_LEFT_LIMIT && (gameObject.getCenter() + this.speed) <= Constants.WORLD_RIGHT_LIMIT) {
            gameObject.setCenter(gameObject.getCenter() + this.speed);
        }
    }

    /**
     * @return the lives of the object.
     */
    public int getLives() {
        return lives;
    }

    /**
     * @param lives sets the lives of the object.
     */
    public void setLives(final int lives) {
        this.lives = lives;
    }

}
